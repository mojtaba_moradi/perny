import axios from "axios";

const instance = axios.create({ baseURL: "https://pernymarket.ir/api/v1" });

export default instance;
