import changeHeader from "./changeHeader.js";
import routingHandle from "./routingHandle.js";
import useWindowSize from "./useWindowSize";
const hoc = { changeHeader, routingHandle, useWindowSize };
export default hoc;
