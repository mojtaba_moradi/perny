import React, { useEffect, useState, useRef } from "react";
import website from "../../website";
import { useRouter } from "next/router";
import useOnScreen from "../../lib/useOnScreen";
import CategoryScreen from "../../website/screens/CategoryScreen";
const Categories = ({ res, filter }) => {
  // console.log({ getInitialProps: res, filter });
  const router = useRouter();
  // ================================================== States
  const [catData, setCatData] = useState(res);
  const [categoryMenu, setCategoryMenu] = useState(false);
  const [filterState, setFilterState] = useState("");
  const [categoryState, setCategoryState] = useState(res?.category);
  const [acceptedCat, setAcceptedCat] = useState();
  const [isLoading, setIsLoading] = useState(false);
  // ================================================== End States
  console.log({ catData, filterState, categoryState, acceptedCat });
  useEffect(() => {
    setFilterState(filter);
  }, [filter]);
  useEffect(() => {
    setCatData(res);
  }, [res]);
  useEffect(() => {
    let findCat;
    res?.allCat?.map((cat) => {
      // console.log(cat.name + "==>" + catData?.category);
      if (catData?.category === cat.name) findCat = cat;
      // else if (res?.category === cat.name) setAcceptedCat(cat);
    });
    if (findCat) {
      setAcceptedCat(findCat);
    } else {
      setAcceptedCat("");
      setCategoryState("");
    }
  }, [catData]);
  const seeProducts = useRef(null);

  // ===========================handlers=======================
  const resDate = async (e, page) => {
    console.log({ e, page });
    return await website.userApi.get.category({ id: e?.id || e?._id, page: page || 1, filter: filterState });
  };

  const categoryClickHandler = async (e, page) => {
    // console.log({ e, page });
    setCategoryMenu(false);
    setIsLoading(true);
    const getNewData = await resDate(e, page);
    setCatData(getNewData);
    setIsLoading(false);
    setCategoryState(e?.name || catData?.category);
    if (categoryState) setFilterState(null);
  };
  //  filters
  const filterClickHandler = async (e) => {
    setCategoryMenu(false);
    setIsLoading(true);
    const getNewFilters = await website.userApi.get.category({ id: acceptedCat?._id, page: 1, filter: e?.id });
    setCatData(getNewFilters);
    setIsLoading(false);
    setFilterState(e?.name);
  };

  // category data remover
  const catDataRemoverFilters = () => {
    if (filter && !acceptedCat) return;
    setFilterState(null);
    categoryClickHandler(acceptedCat);
  };
  const catDataRemoverCategories = () => {
    setCatData(res);
    setFilterState(filter || null);

    if (filter && acceptedCat) {
      setCategoryState(null);
      setAcceptedCat(null);
    } else {
      setCategoryState(res?.category);
    }
  };
  let oldCatData = { ...catData };

  const seeAllProductThisPage = async () => {
    // =============== Old Data
    let oldCatDataCat = { ...oldCatData?.cat };
    let oldCatDataCatDocs = oldCatDataCat.docs;
    if (oldCatData?.cat?.pages > oldCatData?.cat?.page && !isLoading) {
      let resCatData = await resDate(acceptedCat, +oldCatData?.cat.page + 1);
      // =============== New Data
      let newCatDataCat = { ...resCatData?.cat };
      let newCatDataCatDocs = newCatDataCat.docs;
      oldCatDataCat = { ...oldCatDataCat, ["page"]: newCatDataCat?.page, ["docs"]: [...oldCatDataCatDocs, ...newCatDataCatDocs] };
      oldCatData = { ...oldCatData, ["cat"]: oldCatDataCat };
      setCatData(oldCatData);
      setIsLoading(false);
    }
  };
  const screen = useOnScreen(seeProducts, "300px");
  useEffect(() => {
    if (screen && !isLoading) seeAllProductThisPage();
    console.log("end");
  }, [screen]);

  // ===========================handlers=======================

  return <CategoryScreen {...{ categoryClickHandler, res, seeProducts, isLoading, seeAllProductThisPage, setCategoryMenu, categoryMenu, catData, filterState, catDataRemoverFilters, catDataRemoverCategories, categoryState, acceptedCat, filterClickHandler }} />;
};
Categories.getInitialProps = async (props) => {
  console.log({ p: props.ctx.query });
  const res = await website.userApi.get.category({
    id: props.ctx.query?.id,
    filter: props.ctx.query?.filter,
    page: 1,
  });
  return { res, filter: props.ctx.query?.filter };
};

Categories.websiteLayout = true;
export default Categories;
