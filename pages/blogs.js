import Blogs from "../website/screens/articlesScreen/Blogs";
import { GetBlogs } from "../website/userApi/get/blogs";
import { useState, useEffect, useRef } from "react";
import CategoryPanelContainer from "../website/screens/CategoryScreen/CategoryPanel/CategoryPanelContainer";
import website from "../website";
import TopFillterPanel from "../website/screens/CategoryScreen/topFilterPanel";
import LoadingCircelDot from "../panelAdmin/component/UI/Loadings/LoadingCircelDot";
import BackgrandCover from "../panelAdmin/component/UI/BackgrandCover";
import useOnScreen from "../lib/useOnScreen";

const BlogsPage = ({ res }) => {
  console.log({ serverBlogCat: res });

  // ================================================== states
  const [blogData, setBlogData] = useState(res);
  const [categoryMenu, setCategoryMenu] = useState(false);
  const [categoryState, setCategoryState] = useState();
  const [filterState, setFilterState] = useState("");
  const [acceptedCat, setAcceptedCat] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [blogCategories, setBlogCategories] = useState([]);
  // ================================================== End States
  console.log({ blogData, categoryState, acceptedCat });
  // ================================================== constant
  // ========================= const
  const seeAllBlog = useRef(null);
  const screen = useOnScreen(seeAllBlog, "100px");
  // ========================= Let
  let oldBlogData = { ...blogData };
  // ================================================== End constant

  // ================================================== Functions
  // ========================= response api
  const resDate = async (e, page) => {
    console.log({ resData: e, page });
    let resData;
    if (e === "" || e?.id === "" || !e) {
      resData = await GetBlogs("/blog?page=" + page);
      resData = resData?.data;
      console.log("omad");
      console.log({ resData1: resData });
    } else {
      resData = await website.userApi.get.blogCategory({ id: e?.id || e?._id || e, page: page || 1, filter: filterState });
      console.log({ resData2: resData });
    }
    console.log({ resData });
    return resData;
  };
  // ========================= category click add
  const categoryClickHandler = async (e, page) => {
    setCategoryMenu(!categoryMenu);
    // console.log(e, page);
    setIsLoading(true);
    const getNewData = await resDate(e?.id, page || 1);
    // console.log({ getNewData });
    setBlogData(getNewData);
    setIsLoading(false);
    setCategoryState(e.name);
    setAcceptedCat(e);
  };
  // ========================= category click remove
  const blogDataRemoverCategories = () => {
    if (categoryState !== "همه") {
      setBlogData(res);
      setCategoryState("همه");
    }
  };
  const seeAllProductThisPage = async () => {
    // =============== Old Data
    let oldBlogDataCat;
    if (blogData?.blogs) oldBlogDataCat = { ...oldBlogData?.blogs };
    else if (blogData?.allBlogs) oldBlogDataCat = { ...oldBlogData?.allBlogs };

    let oldBlogDataCatDocs = oldBlogDataCat.docs;
    // console.log("faz yek", oldBlogData.allBlogs);
    // console.log({ pages: oldBlogData?.allBlogs?.pages, page: oldBlogData?.allBlogs?.page });
    if ((oldBlogData?.allBlogs?.pages || oldBlogData?.blogs?.pages) > (oldBlogData?.allBlogs?.page || oldBlogData?.blogs?.page) && !isLoading) {
      // console.log("faz do");
      // let resBlogData = await GetBlogs("/blog?page=" + +oldBlogData?.allBlogs.page + 1);
      // resBlogData = resBlogData?.data;
      let resBlogData = await resDate(acceptedCat, +oldBlogData?.allBlogs?.page + 1 || +oldBlogData?.blogs?.page + 1);
      // =============== New Data
      console.log({ resBlogData });
      if (resBlogData) {
        let newBlogDataCat;
        if (resBlogData?.blogs) newBlogDataCat = { ...resBlogData?.blogs };
        else if (resBlogData?.allBlogs) newBlogDataCat = { ...resBlogData?.allBlogs };
        let newBlogDataCatDocs = newBlogDataCat?.docs;
        console.log({ newBlogDataCat, newBlogDataCatDocs });
        oldBlogDataCat = { ...oldBlogDataCat, ["page"]: newBlogDataCat?.page, ["docs"]: [...oldBlogDataCatDocs, ...newBlogDataCatDocs] };
        if (resBlogData?.blogs) oldBlogData = { ...oldBlogData, ["blogs"]: oldBlogDataCat, ["allBlogs"]: "" };
        else if (resBlogData?.allBlogs) oldBlogData = { ...oldBlogData, ["allBlogs"]: oldBlogDataCat, ["blogs"]: "" };

        setBlogData(oldBlogData);
        setIsLoading(false);
      }
    }
  };
  // ================================================== End functions

  // ================================================== useEffect
  // ========================= set category
  // useEffect(() => {
  //   // let findCat;
  //   // res?.blogCategories?.map((cat) => {
  //   // console.log(cat.name + "==>" + blogData?.category);
  //   //   if (blogData?.category === cat.name) findCat = cat;
  //   // });
  //   // if (findCat) {
  //   //   setAcceptedCat(findCat);
  //   // } else {
  //   //   setAcceptedCat("");
  //   //   setCategoryState("");
  //   // }
  // }, [blogData]);
  // ========================= check screen
  useEffect(() => {
    if (screen && !isLoading) seeAllProductThisPage();
    // console.log("end");
  }, [screen]);
  useEffect(() => {
    if (!blogCategories?.length) {
      if (res) setBlogCategories([{ _id: "", name: "همه" }, ...res?.blogCategories]);
      setCategoryState("همه");
    }
  }, []);
  // ================================================== End useEffect

  return (
    <div className="base-container">
      <BackgrandCover onClick={() => setCategoryMenu(!categoryMenu)} fadeIn={isLoading} />
      {isLoading && (
        <div className="staticStyle bgDark">
          <LoadingCircelDot />
        </div>
      )}
      <section className="categories-section">
        <div className="left-side-categories">
          <div className="cat-menu-toggler" onClick={() => setCategoryMenu(!categoryMenu)}>
            <p>دسته بندی ها</p>
          </div>
          <div className="top-filter-cat-section">
            <TopFillterPanel click={blogDataRemoverCategories} datas={categoryState} />
          </div>
          <Blogs datas={blogData?.allBlogs?.docs || blogData?.blogs?.docs} />
          <span ref={seeAllBlog}></span>
        </div>
        <div className="right-side-categories">
          <div className={categoryMenu ? "cat-fil-wrapper show-cat" : "cat-fil-wrapper"}>
            <CategoryPanelContainer activeTitle={categoryState} datas={blogCategories} click={categoryClickHandler} />
          </div>
        </div>
      </section>
    </div>
  );
};

BlogsPage.getInitialProps = async (props) => {
  const res = await GetBlogs("/blog?page=1");
  return { res: res.data };
};
BlogsPage.websiteLayout = true;
export default BlogsPage;
