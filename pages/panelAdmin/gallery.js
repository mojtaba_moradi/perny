import React, { useEffect, useContext, useState, Fragment, useRef } from "react";
import GalleryScreen from "../../panelAdmin/screen/Gallery/GalleryScreen";
import reducer from "../../_context/reducer";
import panelAdmin from "../../panelAdmin";
import useSWR, { mutate, trigger } from "swr";
import useApiRequest from "../../lib/useApiRequest";
import globalUtils from "../../globalUtils";
import SpinnerRotate from "../../panelAdmin/component/UI/Loadings/SpinnerRotate";

// const axios = globalUtils.axiosBase;
// const fetcher = (url) => axios(url).then((r) => r.json());

const gallery = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);

  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue, isServer } = props;
  const [loadingApi, setLoadingApi] = useState(true);
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [currentFilter, setCurrentFilter] = useState("null");
  const filterPrev = useRef(null);
  console.log({ searchData, currentFilter, state });
  useEffect(() => {
    if (currentFilter && currentFilter !== "null") filterPrev.current = currentFilter;
  }, [currentFilter]);

  let resDataView = searchData || state;

  const CurrentPage = state?.page || "1";
  let searchTitle = useRef(null);
  let resTitle;
  if (!resDataView && loadingApi) resTitle = "در حال بارگزاری اطلاعات ...";
  else if (!resDataView?.docs?.length) resTitle = "موردی یافت نشد ...";
  const filters = [
    { value: "null", title: "همه" },
    { value: "product", title: "محصول", disable: searchData },
    { value: "slider", title: "اسلایدر", disable: searchData },
    { value: "category", title: "دسته بندی", disable: searchData },
    { value: "blog", title: "مقاله", disable: searchData },
  ];
  // ======================================================== SWR
  useEffect(() => {
    !acceptedCardInfo && dispatch.changePageName("گالری");
    apiPageFetch("1");
  }, []);
  // ////console.log({ resData });

  const apiPageFetch = async (page, filter) => {
    console.log({ page, filter });
    setLoadingApi(true);
    if (searchData) setSearchData(false);
    const res = await panelAdmin.api.get.gallery({ page: (filter && "1") || page || CurrentPage, filter: filter || currentFilter });
    console.log({ res });
    setState(res);

    setLoadingApi(false);
  };
  // console.log({ resDataView, searchData });

  const onDataSearch = async (page, value, filter) => {
    console.log({ page, value, filter });
    if (value && !page) searchTitle.current = value;
    else if (!page && !value) searchTitle.current = "";
    const resDataSearch = await panelAdmin.api.get.gallery({ search: value || searchTitle.current, page: page || CurrentPage, filter });
    console.log({ resDataSearch, resDataView });
    console.log({ searchData, state });

    if (!value) {
      if (!page) {
        setCurrentFilter(filterPrev.current);

        setSearchData(false);
        return;
      }
    }
    setSearchData(resDataSearch);
    setCurrentFilter("null");
  };
  return (
    <Fragment>
      <GalleryScreen {...{ setCurrentFilter, currentFilter, resTitle, filters, onDataSearch, acceptedCardInfo }} FilteringClick={apiPageFetch} requestData={resDataView} apiPageFetch={searchData ? onDataSearch : apiPageFetch} />
      {loadingApi ? (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
};
gallery.panelAdminLayout = true;
// ========================================= getInitialProps
// gallery.getInitialProps = async (props) => {
//   const { store, isServer } = props.ctx;
//   const res = await panelAdmin.api.get.gallery({ page: "1" });
//   const resData = res.data;

//   return { resData, isServer };
// };

export default gallery;
