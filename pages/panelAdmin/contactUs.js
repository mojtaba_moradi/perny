import React, { useEffect, useState, useContext, useRef } from "react";
import panelAdmin from "../../panelAdmin";
import AddContactUs from "../../panelAdmin/screen/ContactUs/AddContactUs";
import reducer from "../../_context/reducer";

const contactUs = (props) => {
  const strings = panelAdmin.values.apiString;
  const Context = reducer.panelAdminReducer.optionReducerContext;
  const giveContextData = useContext(Context);
  const { dispatch } = giveContextData;
  const { acceptedCardInfo, parentTrue } = props;
  const [state, setState] = useState(false);
  const [searchData, setSearchData] = useState(false);
  const [loadingApi, setLoadingApi] = useState(true);
  const CurrentPage = state?.page || "1";
  const [isSearchData, setIsSearchData] = useState(false);
  let searchTitle = useRef(null);
  useEffect(() => {
    dispatch.changePageName("تماس با ما");
    apiPageFetch();
  }, []);
  const apiPageFetch = async (page) => {
    setLoadingApi(true);
    const res = await panelAdmin.api.get.contactUs(page || CurrentPage);
    setState(res);
    setLoadingApi(false);
  };
  return (
    <>
      <AddContactUs editData={state} apiPageFetch={apiPageFetch} />
    </>
  );
  // return true;
};

contactUs.panelAdminLayout = true;

export default contactUs;
