import React, { useEffect } from "react";
import { useRouter } from "next/router";
import panelAdmin from "../../panelAdmin";

const PanelAdmin = (props) => {
  const router = useRouter();

  useEffect(() => {
    if (!panelAdmin.utils.authorization()) router.push("/panelAdmin/login");
    else router.push("/panelAdmin/dashboard");
  }, []);

  // useEffect(() => {
  //   // The counter changed!
  // }, [router.query.counter]);
  return <div></div>;
};

export default PanelAdmin;
