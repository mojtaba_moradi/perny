import axios from "axios";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import ProductScreen from "../../website/screens/productScreen";

const ProductPage = ({ res }) => {
  const router = useRouter();
  const [resData, setResData] = useState(res || null);
  console.log({ resData });
  useEffect(() => {
    resApi();
  }, [router]);
  const resApi = async () => {
    await axios.get(`https://pernymarket.ir/api/v1/product/${router.query.productId}`).then((res) => {
      setResData(res.data);
    });
  };
  return <ProductScreen {...{ resData }} />;
};

ProductPage.getInitialProps = async (props) => {
  const res = await axios.get(`https://pernymarket.ir/api/v1/product/${props.ctx.query.productId}`);
  return { res: res.data };
};

ProductPage.websiteLayout = true;

export default ProductPage;
