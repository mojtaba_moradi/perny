import SingleBlog from "../../website/screens/articlesScreen/SingleBlog";
import Axios from "axios";

const SingleBlogPage = ({res}) => {
  return ( <SingleBlog datas = {res}/> );
}

SingleBlogPage.getInitialProps = async (props) => {
  const res = await Axios.get(
    `https://pernymarket.ir/api/v1/blog/${props.ctx.query.id}`
  );
  return { res: res.data };
};

SingleBlogPage.websiteLayout = true;

 
export default SingleBlogPage;