import React, { useEffect, useRef, useState } from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { WithUserAgentProps, withUserAgent } from "next-useragent";
import globalUtils from "../../globalUtils";
const AwesomeScroll = (props) => {
  const { data, Row, ua, scrollBar, mouseMove } = props;
  // const[sliderPosition,setSliderPosition]=use
  // ////console.log({ data, Row });
  // ////console.log({ ua });
  // const [scrollLeft, setScrollLeft] = useState();
  const [arrow, setArrow] = useState();
  const size = globalUtils.hoc.useWindowSize();
  const scrollRef = useRef(null);
  const rightArrowRef = useRef(null);
  const leftArrowRef = useRef(null);
  const scrollLeft = useRef(arrow);
  // ////console.log({ AwesomeScroll: data });
  useEffect(() => {
    setArrow(scrollRef?.current?.scrollWidth - scrollRef?.current?.clientWidth);
    scrollLeft.current = scrollRef?.current?.scrollWidth - scrollRef?.current?.clientWidth;
  }, [size]);
  useEffect(() => {
    scrollLeft.current = scrollRef?.current?.scrollWidth - scrollRef?.current?.clientWidth;
  });
  useEffect(() => {
    if (scrollRef.current && mouseMove) {
      let slider;
      if (scrollBar) slider = scrollRef.current.children[0];
      else slider = scrollRef.current;
      // console.log({ slider });

      let isDown = false;
      let startX;
      let scrollLeft;

      slider.addEventListener("mousedown", (e) => {
        isDown = true;
        // slider.classList.add("active");
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
      });

      slider.addEventListener("mouseleave", (e) => {
        isDown = false;
        // slider.classList.remove("active");
        ////console.log("mouseleave");
      });

      slider.addEventListener("mouseup", () => {
        isDown = false;
        // slider.classList.remove("active");
      });

      slider.addEventListener("mousemove", (e) => {
        if (!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = x - startX; //scroll-fast
        // ////console.log({ x, walk }, (slider.scrollLeft = scrollLeft - walk));

        slider.scrollLeft = scrollLeft - walk;
      });
    }
  }, []);

  const _handelScrollTo = (road) => {
    // console.log({ current: scrollRef.current, scrollWidth: scrollRef.current.scrollWidth, scrollLeft: scrollRef.current.scrollLeft });
    if (road === "left")
      scrollRef.current.scrollTo({
        left: scrollRef.current.scrollLeft - scrollRef.current.children[0].clientWidth,
        behavior: "smooth",
      });
    if (road === "right")
      scrollRef.current.scrollTo({
        left: scrollRef.current.scrollLeft + scrollRef.current.children[0].clientWidth,
        behavior: "smooth",
      });
  };
  const handleOnScroll = (e) => {
    rightArrowRef.current.style.zIndex = "10";
    leftArrowRef.current.style.zIndex = "10";
    if (scrollLeft.current === e.currentTarget.scrollLeft) {
      // alert("right");
      rightArrowRef.current.style.zIndex = "-10";
    } else if (e.currentTarget.scrollLeft === 0) {
      leftArrowRef.current.style.zIndex = "-10";
    }
    // scrollLeft.current = scrollRef?.current?.scrollWidth - scrollRef?.current?.clientWidth;

    console.log({ scrollLeft: scrollLeft.current, currentTarget: e.currentTarget.scrollLeft });
  };
  console.log({ scrollLeft: scrollLeft.current });
  return (
    <div className={`${ua && ua.isMobile ? "isMobile" : ""}`} style={{ display: "flex", position: "relative", width: "100%" }}>
      <div ref={rightArrowRef} style={{ display: !scrollLeft?.current && "none", zIndex: -10 }} className="slider-btn btn-right transition0-3 ">
        <span className="centerAll transition0-3" onClick={() => _handelScrollTo("right")}>
          {/* {"<"}
           */}
          <i className={"fas fa-angle-right"} />
        </span>
      </div>
      {!scrollBar ? (
        <ul onScroll={handleOnScroll} onMouseDown={(e) => e.preventDefault()} ref={scrollRef} className="awesome-scroll-container">
          {props.children}
        </ul>
      ) : ua && ua.isMobile ? (
        <ul ref={scrollRef} className="awesome-scroll-container">
          {props.children}
        </ul>
      ) : (
        <ul ref={scrollRef} className="awesome-scroll-container">
          <PerfectScrollbar>{props.children}</PerfectScrollbar>
        </ul>
      )}
      <div style={{ display: !scrollLeft?.current && "none" }} ref={leftArrowRef} className="slider-btn btn-left transition0-3">
        <span className="centerAll transition0-3" onClick={() => _handelScrollTo("left")}>
          <i className={"fas fa-angle-left"} />
          {/* {">"} */}
        </span>
      </div>
    </div>
  );
};
AwesomeScroll.getInitialProps = async (ctx) => {
  return { useragent: ctx.ua.source };
};
export default withUserAgent(AwesomeScroll);
