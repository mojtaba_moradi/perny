import addCategory from "./addCategory";
import addProduct from "./addProduct";
import addGallery from "./addGallery";
import addBanner from "./addBanner";
import addSlider from "./addSlider";
import addBlogCategory from "./addBlogCategory";
import addArticle from "./addArticle";
import addAdmin from "./addAdmin";
import addContactUs from "./addContactUs";

const states = {
  addCategory,
  addProduct,
  addGallery,
  addBanner,
  addSlider,
  addBlogCategory,
  addArticle,
  addAdmin,
  addContactUs,
};
export default states;
