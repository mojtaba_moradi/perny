const addArticle = {
  Form: {
    title: {
      label: "عنوان :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "عنوان",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    // author: {
    //   label: "کاربر :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "کاربر",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    // product: {
    //   label: "محصول :",
    //   elementType: "inputDropDownSearch",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "محصول",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    blogCategory: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    // timeToRead: {
    //   label: "زمان مطالعه :",

    //   elementType: "input",
    //   elementConfig: {
    //     type: "text",
    //     placeholder: "زمان مطالعه",
    //   },
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },

    thumbnail: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    // images: {
    //   label: "عکس های مقاله :",
    //   elementType: "InputFileArray",
    //   kindOf: "image",
    //   value: "",
    //   validation: {
    //     required: true,
    //   },
    //   valid: false,
    //   touched: false,
    // },
    content: {
      label: " متن مقاله :",

      elementType: "ckEditor",
      elementConfig: {
        placeholder: " متن مقاله",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addArticle;
