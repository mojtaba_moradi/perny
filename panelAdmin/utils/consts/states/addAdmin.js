const addAdmin = {
  Form: {
    phoneNumber: {
      label: "شماره همراه :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "شماره همراه",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    password: {
      label: "رمز عبور :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "رمز عبور",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addAdmin;
