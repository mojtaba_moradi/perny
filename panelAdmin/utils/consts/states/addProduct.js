const addArtist = {
  Form: {
    name: {
      label: "نام  :",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نام ",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    category: {
      label: "دسته بندی :",
      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },

    realPrice: {
      label: "قیمت اصلی :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت اصلی",
      },
      value: "",
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    newPrice: {
      label: "قیمت جدید :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "قیمت جدید",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: true,
    },
    weight: {
      label: "وزن :",
      elementType: "input",
      elementConfig: {
        type: "number",
        placeholder: "وزن",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    unit: {
      label: "واحد :",
      elementType: "input",
      elementConfig: {
        placeholder: "واحد",
      },
      value: "",
      validation: {
        required: false,
      },
      valid: true,
      touched: false,
    },
    images: {
      label: "عکس :",
      elementType: "InputFileArray",
      kindOf: "image",
      value: [],
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
  },

  formIsValid: false,
};

export default addArtist;
