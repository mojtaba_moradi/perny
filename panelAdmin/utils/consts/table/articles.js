const articles = (data) => {
  const thead = ["#", "عکس", "عنوان ", "عنوان محصول", "نوشته", "زمان مطالعه", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < data.length; index++) {
    let NotEntered = "وارد نشده";
    let thumbnail = data[index]?.thumbnail || NotEntered;
    let title = data[index].title || NotEntered;
    let productName = data[index]?.product?.name || NotEntered;
    let content = { option: { eye: true, name: "content" } };
    let timeToRead = data[index]?.timeToRead || NotEntered;

    tbody.push({ data: [thumbnail, title, productName, content, timeToRead, { option: { edit: true, remove: true } }], style: {} });
  }
  return { thead, tbody };
};

export default articles;
