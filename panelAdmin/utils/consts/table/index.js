import showScenario from "./ShowScenario";
import showOwner from "./ShowOwner";
import ShowDiscount from "./ShowDiscount";
import ShowClub from "./ShowClub";
import showTransaction from "./ShowTransactions";
import transactionDiscount from "./transactionDiscount";
import members from "./members";
import memberTransaction from "./memberTransaction";
import memberDiscount from "./memberDiscount";
import blogCategory from "./blogCategory";
import products from "./products";
import articles from "./articles";
import comments from "./comments";

const table = {
  memberDiscount,
  memberTransaction,
  members,
  transactionDiscount,
  showTransaction,
  ShowClub,
  ShowDiscount,
  showOwner,
  showScenario,
  blogCategory,
  products,
  articles,
  comments,
};
export default table;
