const instrument = (data, acceptedCard) => {
  const cardFormat = [];
  ////console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let name = dataIndex.name ? dataIndex.name : "";
    let url = dataIndex.url ? dataIndex.url : "";
    let images = url;
    // ////console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard === url ? "activeImage" : "") : "",
      image: { value: images },
      body: [
        {
          right: [{ elementType: "text", value: name, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default instrument;
