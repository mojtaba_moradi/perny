import React from "react";

const category = (data) => {
  const cardFormat = [];
  console.log({ data });

  for (let index in data) {
    let dataIndex = data[index];
    let title = dataIndex.name ? dataIndex.name : "";
    let image = dataIndex.image ? dataIndex.image : "";
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: image },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
      ],
    });
  }

  return cardFormat;
};

export default category;
