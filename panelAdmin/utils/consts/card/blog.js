import React from "react";
import PanelString from "../../../value/PanelString";

const blog = (data) => {
  ////console.log({ blogdata: data });

  const cardFormat = [];
  for (let index in data) {
    let title = data[index].title ? data[index].title : "";
    let description = data[index].description ? data[index].description : "";
    let like = data[index].like ? data[index].like : "0";
    let category = data[index].category ? data[index].category : "";
    let categoryTitle = category.title ? category.title : "";

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index].cover },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        {
          right: [{ elementType: "text", value: description, title: description, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
        },
        {
          right: [{ elementType: "text", value: categoryTitle }],
          left: [{ elementType: "icon", value: like, className: "icon-thumbs-up", direction: "ltr", style: { fontSize: "1.4em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default blog;
