import gallery from "./gallery";
import article from "./article";
import product from "./product";
import category from "./category";
import slider from "./slider";
import banner from "./banner";

const card = {
  category,
  gallery,
  product,
  slider,
  banner,
  article,
};
export default card;
