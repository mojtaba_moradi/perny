import panelAdmin from "../../..";
import jalaliDateConvert from "../../jalaliDateConvert";

const article = (data) => {
  const cardFormat = [];

  for (let index in data) {
    let noEntries = panelAdmin.values.strings.NO_ENTRIES;
    let title = data[index].title || noEntries;
    let createdAt = data[index]?.createdAt || false;
    let jalali = jalaliDateConvert(createdAt);
    let jalaliDate = createdAt ? jalali.clock + " - " + jalali.date : noEntries;

    let blogCategoryName = data[index]?.blogCategory?.name || noEntries;
    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      image: { value: data[index]?.thumbnail || noEntries },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } }],
        },
        {
          left: [{ elementType: "text", value: jalaliDate, direction: "ltr", style: { fontSize: "0.67rem", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
        {
          right: [
            {
              elementType: "text",
              value: blogCategoryName,
              title: blogCategoryName,
              style: { color: "#8c8181", fontSize: "0.6em", fontWeight: "500" },
            },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default article;
