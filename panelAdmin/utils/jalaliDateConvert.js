import moment from "jalali-moment";
import timeNow from "./timeNow";
import iranMonthName from "./consts/iranMonthName";
function jalaliDateConvert(str, monthName) {
  // console.log({ str });
  if (str) {
    let date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    let clock = timeNow(date);
    let changeDate = [date.getFullYear(), mnth, day].join("-");
    let dateBasic = moment(changeDate, "YYYY/MM/DD").locale("fa").format("YYYY/MM/DD").toString();
    let dateBasicSplit = dateBasic?.split("/");
    let dateWithMonthName = dateBasicSplit[2] + " " + iranMonthName(dateBasicSplit[1]) + " " + dateBasicSplit[0];
    return { date: monthName ? dateWithMonthName : dateBasic, clock };
  } else return false;
}
export default jalaliDateConvert;
// function dateConvert(str) {
//   if (str) {
//     let mnths = {
//         Jan: "01",
//         Feb: "02",
//         Mar: "03",
//         Apr: "04",
//         May: "05",
//         Jun: "06",
//         Jul: "07",
//         Aug: "08",
//         Sep: "09",
//         Oct: "10",
//         Nov: "11",
//         Dec: "12",
//       },
//       date = str?.split(" ");

//     return [date[3], mnths[date[1]], date[2]].join("-");
//   }
// }
