import panelAdmin from "..";
import values from "../values/index";

const menuFormat = [
  {
    title: "عمومی",
    menus: [
      {
        route: values.routes.GS_ADMIN_DASHBOARD,
        menuTitle: values.fa.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: values.fa.GALLERY,
        menuIconImg: false,
        menuIconClass: "fad fa-images",
        subMenu: [
          {
            title: values.fa.SEE_GALLERIES,
            route: values.routes.GS_ADMIN_GALLERY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.fa.CATEGORY,
        menuIconImg: false,
        menuIconClass: "fad fa-cubes",
        subMenu: [
          {
            title: values.fa.SEE_CATEGORIES,
            route: values.routes.GS_ADMIN_CATEGORY,
          },
          {
            title: values.fa.ADD_CATEGORY,
            route: values.routes.GS_ADMIN_ADD_CATEGORY,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.fa.PRODUCT,
        menuIconImg: false,
        menuIconClass: "fad fa-store",
        subMenu: [
          {
            title: values.fa.SEE_PRODUCTS,
            route: values.routes.GS_ADMIN_PRODUCT,
          },
          {
            title: values.fa.ADD_PRODUCT,
            route: values.routes.GS_ADMIN_ADD_PRODUCT,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.fa.SLIDER,
        menuIconImg: false,
        menuIconClass: "fas fa-presentation",
        subMenu: [
          {
            title: values.fa.SEE_SLIDERS,
            route: values.routes.GS_ADMIN_SLIDER,
          },
          {
            title: values.fa.ADD_SLIDER,
            route: values.routes.GS_ADMIN_ADD_SLIDER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.fa.BANNER,
        menuIconImg: false,
        menuIconClass: "far fa-archive",
        subMenu: [
          {
            title: values.fa.SEE_BANNERS,
            route: values.routes.GS_ADMIN_BANNER,
          },
          {
            title: values.fa.ADD_BANNER,
            route: values.routes.GS_ADMIN_ADD_BANNER,
          },
        ],
      },
      {
        route: false,
        menuTitle: values.fa.ARTICLES,
        menuIconImg: false,
        menuIconClass: "fad fa-sticky-note",
        subMenu: [
          {
            title: values.fa.BLOG_CATEGORY,
            route: values.routes.GS_ADMIN_SEE_BLOG_CATEGORY,
          },
          {
            title: values.fa.SHOW_ARTICLES,
            route: values.routes.GS_ADMIN_SEE_ARTICLE,
          },
        ],
      },
      {
        route: values.routes.GS_ADMIN_COMMENT,
        menuTitle: values.fa.COMMENT,
        menuIconImg: false,
        menuIconClass: "fad fa-comments",
        subMenu: [],
      },
      {
        route: false,
        menuTitle: values.fa.ADMIN,
        menuIconImg: false,
        menuIconClass: "fad fa-user-shield",
        subMenu: [
          // {
          //   title: values.fa.SEE_ADMIN,
          //   route: values.routes.GS_ADMIN_SEE_ADMIN,
          // },
          {
            title: values.fa.ADD_ADMIN,
            route: values.routes.GS_ADMIN_ADD_ADMIN,
          },
        ],
      },
      {
        route: values.routes.GS_ADMIN_CONTACT_US,
        menuTitle: values.fa.CONTACT_US,
        menuIconImg: false,
        menuIconClass: "fad fa-comments",
        subMenu: [],
      },
      // {
      //   route: values.routes.GS_ADMIN_ABOUT_US,
      //   menuTitle: values.fa.ABOUT_US,
      //   menuIconImg: false,
      //   menuIconClass: "fad fa-comments",
      //   subMenu: [],
      // },
    ],
  },
];

export default menuFormat;
