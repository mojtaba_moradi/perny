const TRACKS = "آهنگ ها ";
const NO_ENTRIES = "وارد نشده";
const FOLLOWERS = "دنبال کنندگان";
const global = { TRACKS, NO_ENTRIES: NO_ENTRIES, FOLLOWERS };
export default global;
