import React, { useEffect, useState, Fragment, useRef } from "react";
import AddGallery from "../AddGallery";
import { Modal, Button } from "react-bootstrap";
import panelAdmin from "../../..";
import _ from "lodash";
import MyVerticallyCenteredModal from "../../../component/UI/Modals/MyVerticallyCenteredModal";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import globalUtils from "../../../../globalUtils";
import PaginationM from "../../../component/UI/PaginationM";
import utils from "../../../utils";
import ModalStructure from "../../../component/UI/Modals/ModalStructure";
import InputVsIcon from "../../../component/UI/InputVsIcon";
import GalleryFilter from "./GalleryFilter";
const axios = globalUtils.axiosBase;

const GalleryScreen = (props) => {
  const states = panelAdmin.utils.consts.states;
  const card = panelAdmin.utils.consts.card;

  const { FilteringClick, currentFilter, setCurrentFilter, onDataSearch, apiPageFetch, filters, acceptedCardInfo, requestData, resTitle } = props;
  const [showUploadModal, setUploadModal] = useState(false);
  const [data, setData] = useState({ ...states.addGallery });
  const [searchTitle, setSearchTitle] = useState("");
  const [modalDetails, setModalDetails] = useState({
    show: false,
    kindOf: null,
    name: null,
    removeId: null,
    editId: null,
    editData: [],
  });
  const [removeLoading, setRemoveLoading] = useState(false);

  // const CurrentPage = info?.page || "1";
  const strings = panelAdmin.values.apiString;
  // const URL = strings.IMAGE + "/" + CurrentPage;

  // ========================================================= remove data with data id
  const reqApiRemove = async (id) => {
    setRemoveLoading(true);

    if (await panelAdmin.api.deletes.gallery(id)) {
      apiPageFetch(requestData.page, currentFilter);
      onHideModal();
    }
    setRemoveLoading(false);
  };
  // ========================================================= modal
  const _onSubmit = async (e) => {
    const formData = {};
    for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    // trigger(URL);
    console.log({ formData });
    apiPageFetch(requestData.page, currentFilter);
    setData({ ...states.addGallery });
  };
  // ================================== modal close
  const onHideModal = () => {
    setModalDetails({ ...modalDetails, show: false, kindOf: false, removeId: null });
  };
  // ================================== modal open
  const onShowModal = (event) => {
    setModalDetails({ ...modalDetails, show: true, kindOf: event.kindOf, name: event?.name, editData: event?.editData, removeId: event?.removeId });
  };
  // ================================== handel modal end work
  const modalRequest = async (bool) => {
    if (bool) reqApiRemove(modalDetails.removeId);
    else onHideModal();
  };
  // ========================================================= END modal
  const modalData = {
    size: "lg",
    status: showUploadModal,
    setStatus: setUploadModal,
    title: "آپلود عکس",
    acceptedBtn: "ثبت",
    acceptedDisabled: !data.formIsValid,
    onSubmit: _onSubmit,
  };
  const optionClick = ({ _id, mission, index }) => {
    ////console.log({ _id, mission });
    switch (mission) {
      case "remove":
        onShowModal({ kindOf: "question", removeId: _id });
        break;
      case "edit":
        onShowModal({ kindOf: "component", editData: requestData.docs[index] });
        break;
      default:
        break;
    }
  };

  const searchData = (e) => {
    setSearchTitle(e.target.value);
    onDataSearch("", e.target.value, currentFilter);
  };

  let dropDownData = [];
  for (const index in filters)
    dropDownData.push({
      value: filters[index].value,
      title: filters[index].title,
    });
  const onFilterClick = (filterName) => {
    console.log({ filterName });
    setCurrentFilter(filterName);
    FilteringClick(requestData.page, filterName);
    setSearchTitle("");
  };
  // ========================================  CARD ELEMENT
  const showDataElement = <ShowCardInformation optionClick={optionClick} options={{ remove: true, copy: true }} data={card.gallery(requestData?.docs, acceptedCardInfo?.acceptedCard)} onClick={null} acceptedCardInfo={acceptedCardInfo} />;
  return (
    <div className="gallery">
      <ModalStructure modalRequest={modalRequest} reqApiRemove={reqApiRemove} onHideModal={onHideModal} modalDetails={modalDetails} loading={removeLoading} />
      <MyVerticallyCenteredModal {...modalData}>
        <AddGallery data={data} setData={setData} />
      </MyVerticallyCenteredModal>

      <div className="gallery-header-wrapper">
        <div className="gallery-header">
          <InputVsIcon name="genreTitle" icon="far fa-search" placeholder="جستجو..." value={searchTitle} onChange={searchData} dir="ltr" />

          <Button onClick={() => setUploadModal(true)} className="">
            {"افزودن عکس"}
          </Button>
        </div>
        <GalleryFilter dropDownData={dropDownData} currentFilter={currentFilter} filters={filters} onFilterClick={onFilterClick} />

        {!resTitle ? showDataElement : <div style={{ marginTop: "30px" }}>{resTitle}</div>}
        <PaginationM limited={"4"} pages={requestData?.pages} activePage={requestData?.page} onClick={apiPageFetch} />
      </div>
    </div>
  );
};
export default GalleryScreen;
