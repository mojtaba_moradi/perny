import React, { useState } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import panelAdmin from "../../../..";
import { ButtonGroup, Button } from "react-bootstrap";
const FormInputGallery = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;

  const dropDown = panelAdmin.utils.consts.galleryConstants();
  let dropDownData = [];
  for (const index in dropDown)
    dropDownData.push({
      value: dropDown[index].value,
      title: dropDown[index].title,
    });
  const imageDisable = () => {
    panelAdmin.utils.toastify("لطفا موارد بالا را تکمیل نمایید", "error");
  };
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      {stateArray.map((formElement) => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, progress;
        let disable = true;
        let imageTypeValue = stateArray[0].config.value;
        let imageNameValue = stateArray[1].config.value;
        if (imageTypeValue) disable = false;

        changed = (e) =>
          inputChangedHandler({
            value: e.currentTarget.value,
            name: formElement.id,
            type: e.currentTarget.type,
            files: e.currentTarget.files,
          });
        accepted = (value) => inputChangedHandler({ value: value, name: formElement.id });
        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        else if (formElement.id === "image") {
          progress = state.progressPercentImage;
          accepted = imageDisable;
          if (!imageNameValue) disable = true;
        }
        console.log({ imageNameValue });
        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={(index) => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={progress}
            checkSubmited={checkSubmited}
            dropDownData={dropDownData}
            disabled={disable}
          />
        );
        if (formElement.id === "imageType") {
          form = (
            <div className={"Input"}>
              <label className={"Label"}>{formElement.config.label}</label>
              <ButtonGroup style={{ direction: "ltr", width: "100%" }} aria-label="Basic example">
                {formElement.config.childValue.map((child, index) => {
                  return (
                    <Button
                      disabled={""}
                      onClick={() => inputChangedHandler({ value: child.value, name: formElement.id }, { initialState: true })}
                      style={{
                        backgroundColor: formElement.config.value === child.value ? "#07a7e3" : "",
                      }}
                      key={index}
                      variant="secondary"
                    >
                      {child.name}{" "}
                    </Button>
                  );
                })}
              </ButtonGroup>
            </div>
          );
        }
        return form;
      })}
    </form>
  );
};

export default FormInputGallery;
