import React, { useState, useEffect } from "react";
import Inputs from "../../../../component/UI/Inputs/Input";
import { get } from "../../../../api";
import panelAdmin from "../../../..";
const FormInputContactUs = (props) => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited, showModal } = props;
  const accept = (event) => inputChangedHandler(event);
  // =========================================================================== SEARCH STRUCTURE FOR DROP DOWN
  // =========================================================================== FORM
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      {stateArray.map((formElement) => {
        // ================================= VARIABLE VALUES
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        const inputClasses = ["InputElement"];
        // ================================= CHANGE ELEMENT AMOUNTS
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");
        // ================================= INPUTS
        let FormManagement = panelAdmin.utils.operation.FormManagement({ formElement, showModal, inputChangedHandler, accept, removeHandel });
        let form = <Inputs invalid={invalid} shouldValidate={shouldValidate} touched={touched} {...FormManagement} />;

        return form;
      })}
    </form>
  );
};

export default FormInputContactUs;
