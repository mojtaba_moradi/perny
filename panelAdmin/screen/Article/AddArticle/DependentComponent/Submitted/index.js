import React from "react";

const Submitted = async (props) => {
  const { setSubmitLoading, data, editData, put, post, setData, states, setEdit, propsHideModal } = props;
  setSubmitLoading(true);

  const formData = {};
  for (let formElementIdentifier in data.Form) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
  if (editData) {
    if (await put.product({ id: editData._id, data: formData })) {
      setEdit();
      propsHideModal();
    }
  } else if (await post.product(formData));
  setData({ ...states.addProduct });
  setSubmitLoading(false);
};

export default Submitted;
