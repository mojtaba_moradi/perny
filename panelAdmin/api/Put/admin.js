import axios from "../axios-orders";
import panelAdmin from "../..";

const admin = async (param) => {
  const toastify = panelAdmin.utils.toastify;
  const URL = panelAdmin.values.apiString.LOGIN;

  return axios
    .put(URL, param)
    .then((Response) => {
      console.log({ Response });
      if (Response?.data) toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};
export default admin;
