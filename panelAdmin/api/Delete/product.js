import panelAdmin from "../..";
import axios from "../axios-orders";
// import Axios from "axios";
const product = async (param) => {
  const toastify = panelAdmin.utils.toastify;

  let URL = panelAdmin.values.apiString.PRODUCT;

  return axios
    .delete(URL + "/" + param)
    .then((Response) => {
      ////console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });

      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default product;
