import axios from "../axios-orders";
import panelAdmin from "../..";

const comments = async (page, parentType, isConfirmed) => {
  console.log({ page, parentType, isConfirmed });
  let URL = page ? panelAdmin.values.apiString.COMMENT + "?page=" + page : panelAdmin.values.apiString.COMMENT;
  if (isConfirmed) URL += "&isConfirmed=" + isConfirmed;
  if (parentType) URL += "&parentType=" + parentType;
  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });
      return res?.data;
    })
    .catch((error) => {
      console.log({ error });
      // if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      // else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default comments;
