import axios from "../axios-orders";
import panelAdmin from "../..";

const sliders = async (page, type = "Product") => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  let URL = page ? strings.SLIDER + "?page=" + page : strings.SLIDER;
  if (type) URL += "&type=" + type;
  return axios
    .get(URL)
    .then((sliders) => {
      console.log({ sliders });
      return sliders;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};

export default sliders;
