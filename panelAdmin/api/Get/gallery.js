import axios from "../axios-orders";
import panelAdmin from "../..";

const gallery = async (param) => {
  // ////console.log({ page });

  let URL = panelAdmin.values.apiString.IMAGE + "?page=" + param?.page;
  if (param?.filter) if (param?.filter !== "null") URL += "&filter=" + param?.filter;
  if (param?.search) URL += "&search=" + param?.search;
  return axios
    .get(URL)
    .then((gallery) => {
      console.log({ gallery });
      return gallery?.data;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};

export default gallery;
