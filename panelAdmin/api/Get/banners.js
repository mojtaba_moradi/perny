import axios from "../axios-orders";
import panelAdmin from "../..";

const banners = async (page) => {
  const strings = panelAdmin.values.apiString;
  const axiosData = page ? strings.BANNER + "?page=" + page : strings.BANNER;

  return axios
    .get(axiosData)
    .then((banners) => {
      // console.log({ banners });
      return banners;
    })
    .catch((error) => {
      // console.log({ error });
      return false;
    });
};

export default banners;
