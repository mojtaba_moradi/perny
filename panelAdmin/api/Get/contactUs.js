import axios from "../axios-orders";
import panelAdmin from "../..";

const contactUs = async (page) => {
  let getUrl = panelAdmin.values.apiString.CONTACT_US;

  return axios
    .get(getUrl)
    .then((res) => {
      console.log({ res });
      return res?.data;
    })
    .catch((error) => {
      console.log({ error });
      return false;
    });
};

export default contactUs;
