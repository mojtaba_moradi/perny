import axios from "../../../utils/axiosBase";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const discountSearch = async (param, returnData) => {
  return axios
    .get(Strings.ApiString.DISCOUNT_SEARCH + "/" + param)
    .then((discountSearch) => {
      ////console.log({ discountSearch });
      returnData(discountSearch.data);
      // loading(false);
    })
    .catch((error) => {
      //////console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default discountSearch;
