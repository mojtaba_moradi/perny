// import axiosBase from "../../globalUtils/axiosBase";
import Cookie from "js-cookie";
import axios from "axios";

const instance = axios.create({ baseURL: "https://pernymarket.ir/api/v1" });
instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("PernyAdminToken");

export default instance;
