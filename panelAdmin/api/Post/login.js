import Cookies from "js-cookie";
import panelAdmin from "../..";
import globalUtils from "../../../globalUtils";

const login = async (param, setLoading) => {
  const toastify = panelAdmin.utils.toastify;
  const pageRoutes = panelAdmin.values.routes.GS_ADMIN_DASHBOARD;
  const axios = globalUtils.axiosBase;
  console.log({ param });
  let URL = panelAdmin.values.apiString.LOGIN;

  // setLoading(true);

  return axios
    .post(URL, param)
    .then((Response) => {
      console.log({ token: Response });

      if (Response.data);
      Cookies.set("PernyAdminToken", Response.data.token, { expires: 7 });
      // // window.location = pageRoutes.GS_PANEL_ADMIN_TITLE;
      toastify("شما تایید شده اید", "success");
      setTimeout(() => {
        window.location = pageRoutes;
      }, 1000);
      return true;
    })
    .catch((error) => {
      console.log({ error });
      setLoading(false);

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      if (error.response.data.Error === 1019) toastify("این شماره ثبت نشده است", "error");
      else if (error.response.data.Error === 1099) toastify("این شماره ثبت نشده است", "error");
      else if (error.response.data.Error === 1098) toastify("پسورد شما نامعتبر است", "error");
      else toastify("خطایی در سرور . لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default login;
