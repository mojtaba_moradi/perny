import React from "react";
const LoadingDot1 = ({ width, height }) => {
  return (
    <div className="sk-chase" style={{ width, height }}>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
    </div>
  );
};

export default LoadingDot1;
