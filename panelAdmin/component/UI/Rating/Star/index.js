import React, { useState } from "react";
import Rating from "@material-ui/lab/Rating";
import { makeStyles } from "@material-ui/core/styles";

const Star = (props) => {
  const { rating, fixed, size, change } = props;
  const [Value, setValue] = useState();
  const [Hover, setHover] = useState();
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      flexDirection: "column",
      "& > * + *": {
        marginTop: theme.spacing(1),
      },
    },
  }));
  const classes = useStyles();
  let element = (
    <Rating
      name="half-rating"
      defaultValue={rating || 2.5}
      precision={0.5}
      onChange={(event, newValue) => {
        change(newValue);
      }}
      onChangeActive={(event, newHover) => {
        setHover(newHover);
      }}
    />
  );
  if (fixed) {
    element = <Rating name="half-rating-read" defaultValue={rating} precision={0.5} readOnly size={size} />;
  }
  return (
    <div style={{ direction: "ltr", marginRight: "auto", textAlign: "center", justifyContent: "center", alignItems: "center" }} className={classes.root} size={size}>
      {element}
    </div>
  );
};

export default Star;
