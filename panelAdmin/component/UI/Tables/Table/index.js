import React from "react";
export default function Table({ headers, data }) {
  const Headers = () => {
    return (
      <thead>
        <tr>
          {headers.headers.map((head, index) => (
            <th key={"head-" + index}>{head}</th>
          ))}
        </tr>
      </thead>
    );
  };

  const Rows = () => {
    return (
      <tbody>
        {data?.docs.map((row, index) => {
          return (
            <tr key={"body-" + index}>
              <td>{index + 1}</td>
              {headers.body.map((bod, i) => {
                if (row[bod].includes("http"))
                  return (
                    <td key={"body-" + i}>
                      <img src={row[bod]} />
                    </td>
                  );
                return <td key={"body-" + i}>{row[bod]}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    );
  };

  return (
    <div className="table-container">
      <section>
        <table className="table-content">
          <Headers />
          <Rows />
        </table>
        <div className="table-paginate">
          <i className="fal fa-angle-right"></i>
          <span>1</span>
          <span>2</span>
          <span>3</span>
          <i className="fal fa-angle-left"></i>
        </div>
      </section>
    </div>
  );
}
