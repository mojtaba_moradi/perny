import React from "react";
import LoadingDot1 from "../../Loadings/LoadingDot1";
const ModalTrueFalse = (props) => {
  return (
    <div className="modal-box">
      <div className="m-b-question-title">
        <span>{props.modalHeadline}</span>
        <span>{props.modalQuestion}</span>
      </div>
      <div className="btns-container">
        <div disabled={props.loading} onClick={props.loading ? null : () => props.modalAccept(true)} className="submited-box">
          <a className="btns btns-primary" type="submit">
            {props.loading ? <LoadingDot1 width="1.5rem" height="1.5rem" /> : props.modalAcceptTitle}
          </a>
        </div>
        <div onClick={() => props.modalAccept(false)} className="submited-box">
          <a className="btns btns-warning" type="submit">
            {" "}
            {props.modalCanselTitle}
          </a>
        </div>
      </div>
    </div>
  );
};

export default ModalTrueFalse;
