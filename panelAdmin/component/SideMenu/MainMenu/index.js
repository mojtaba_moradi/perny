import React, { useState, useEffect } from "react";
import MenuTitle from "../MenuTitle";
import Menu from "../Menu";

const MainMenu = ({ mainMenus, windowLocation }) => {
  const [showLi, setShowLi] = useState("");
  const [selectedMenuTitle, setMenuTitle] = useState();
  // ////console.log({ windowLocation });

  useEffect(() => {
    checkMenu();
  }, []);
  const checkMenu = () => {
    mainMenus.map((menu) => {
      return menu.menus.map((menu) => {
        return menu.subMenu.map((subMenu) => {
          if (subMenu.route === "/" + windowLocation.substr(windowLocation.indexOf("panelAdmin"))) {
            setMenuTitle(menu.menuTitle);
            setShowLi(menu.menuTitle);
            return true;
          }
        });
      });
    });
  };
  useEffect(() => {
    checkMenu();
  }, [windowLocation]);
  // //////console.log({ showLi, selectedMenuTitle });

  return mainMenus.map((mainMenu, index) => {
    return (
      <ul key={index + "m"}>
        <MenuTitle title={mainMenu.title} />
        <Menu windowLocation={windowLocation} menus={mainMenu.menus} showLi={showLi} setShowLi={setShowLi} selectedMenuTitle={selectedMenuTitle} setMenuTitle={setMenuTitle} />
      </ul>
    );
  });
};

export default MainMenu;
