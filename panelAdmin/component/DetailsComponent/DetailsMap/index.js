import React, { useState } from "react";
import "./index.scss";
import SimpleExample from "../../SimpleExample";

const DetailsMap = ({ Info, fieldName, label, fixed, sendNewVal, toastify, information }) => {
  const [state, setState] = useState({
    change: false,
    Info: { ...Info },
  });
  const owner = [
    {
      city: "rashty",
      title: information.title,
      address: information.district + " ، " + information.address,
      telephone: information.phone,
      mobile: information.phoneNumber,
      image: information.thumbnail,
      location: information.coordinate,
    },
  ];
  ////console.log({ Info: state.Info });

  const _handelEdit = () => {
    setState((prev) => ({
      ...prev,
      change: !state.change,
      Info: { ...Info },
    }));
  };
  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.Info };
    if (state.Info) sendNewVal(data);
    else toastify("تغییراتی مشاهده نشد", "error");
    _handelEdit();
  };
  const _handelOnchange = (value) => {
    setState({ ...state, Info: value });
  };

  return (
    <div className="card-details-row">
      <div className="about-title">
        <span>{label} :</span>
        {!fixed && !state.change ? <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i> : ""}
        {state.change ? (
          <div className="btns-container">
            <a className="btns btns-success" onClick={_handelSendChanged}>
              {" "}
              ثبت{" "}
            </a>
            <a className="btns btns-warning" onClick={_handelEdit}>
              {" "}
              لغو{" "}
            </a>
          </div>
        ) : (
          ""
        )}
      </div>

      <div className=" "> {state.change ? <SimpleExample newPin center={{ lat: "37.3003561", lng: "49.6029556" }} onChange={_handelOnchange} /> : <SimpleExample data={owner} center={Info} />}</div>
    </div>
  );
};

export default DetailsMap;
