import { galleryData } from "./gallery";
import { categoryData, categorySearchData } from "./categories";
import { productData, productSearchData } from "./products";

const GET = {
  // ================================= GALLERY
  galleryData,
  // ================================= CATEGORY
  categoryData,
  categorySearchData,
  // ================================= PRODUCT
  productData,
  productSearchData,
  //  // ================================= SLIDER
  //  sliderData,
  //  sliderSearchData,
};

export default GET;
