import panelAdmin from "../..";

export const categoryInitialState = {
  categoryData: null,
  searchCategoryData: null,
};

function categoryReducer(state = categoryInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_CATEGORY_DATA:
      return { ...state, ...{ categoryData: action.data } };
    case atRedux.SET_SEARCH_CATEGORY_DATA:
      return { ...state, ...{ searchCategoryData: action.data } };
    default:
      return state;
  }
}

export default categoryReducer;
