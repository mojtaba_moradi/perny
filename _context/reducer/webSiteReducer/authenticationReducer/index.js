import contextParent from "../../..";
import Cookies from "js-cookie";
// ========================================================  REDUCER
export const authenticationReducer = (state, action) => {
  switch (action.type) {
    case "ADD_TOKEN":
      Cookies.set("PernyUserToken", action.payload, { expires: 7 });
      return { ...state, authentic: action.payload };
    case "REMOVE_TOKEN":
      Cookies.remove("PernyUserToken");
      return { ...state, authentic: null };
    case "CHECK_LOGIN":
      return { ...state, authentic: Cookies.get("PernyUserToken") };
    default:
      return state;
  }
};
// ========================================================  DISPATCH
const addToken = (dispatch) => (token) => {
  dispatch({ type: "ADD_TOKEN", payload: token });
};
const removeToken = (dispatch) => () => {
  dispatch({ type: "REMOVE_TOKEN" });
};
const checkLogin = (dispatch) => () => {
  dispatch({ type: "CHECK_LOGIN" });
};
export const { Provider, Context } = contextParent(
  authenticationReducer,
  {
    addToken,
    removeToken,
    checkLogin,
  },
  {
    authentic: null,
  }
);
