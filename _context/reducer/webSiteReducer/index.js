import { Provider as shoppingCartReducerProvider, Context as shoppingCartReducerContext } from "./shoppingCartReducer";
import { Provider as authenticationReducerProvider, Context as authenticationReducerContext } from "./authenticationReducer";
const webSiteReducer = { shoppingCartReducerProvider, shoppingCartReducerContext, authenticationReducerProvider, authenticationReducerContext };

export default webSiteReducer;
