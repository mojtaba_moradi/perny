import axios from "../userBaseUrl";

export const postTest = async (url, data) =>
  axios
    .post(url, data)
    .then((res) => true)
    .catch((er) => ({ error: er }));
