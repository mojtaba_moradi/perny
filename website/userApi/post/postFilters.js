import axios from '../userBaseUrl';

export const postFilters = async (url, data) =>
  axios
    .post(url, data)
    .then((res) => ({ data: res.data }))
    .catch((er) => ({ error: er }));
