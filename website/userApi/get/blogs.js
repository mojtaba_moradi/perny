import axios from "../userBaseUrl";

export const GetBlogs = async (url) =>
  axios
    .get(url)
    .then((res) => ({ data: res.data }))
    .catch((er) => ({ error: er }));
