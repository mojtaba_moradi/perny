import axios from "../userBaseUrl";
import website from "../..";

const category = (param) => {
  console.log({ categoryparam: param });
  let URL = `${website.values.apiString.CATEGORY}`;
  // URL = `${website.values.apiString.CATEGORY + '/' + param.id +'?page='+param.page +'&filter='+param.filter}`;
  if (param) URL += "?";
  if (param?.id) URL += "&id=" + param.id;
  if (param?.filter) URL += "&filter=" + param.filter;
  if (param?.page) URL += "&page=" + param.page;
  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });

      return res?.data;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};

export default category;
