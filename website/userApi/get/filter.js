import axios from "../userBaseUrl";

export const getFilters = async (url) =>
  axios
    .get(url)
    .then((res) => ({ data: res.data }))
    .catch((er) => ({ error: er }));
