import axios from "../userBaseUrl";
import website from "../..";

const comment = (param) => {
  const URL = `${website.values.apiString.COMMENT + "/" + param.id}`;

  return axios
    .get(URL)
    .then((res) => {
      console.log({ res });

      return res?.data;
    })
    .catch((er) => {
      console.log({ er });

      return false;
    });
};

export default comment;
