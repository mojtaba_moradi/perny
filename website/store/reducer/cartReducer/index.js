import atRedux from '../../actionType/redux';

const initialState = {
  cartItems: [],
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case atRedux.ADD_TO_CART:
      //Add Item to cart
      return {
        ...state,
        ...state.cartItems.push({ ...action.data, count: 1 }),
      };

    case atRedux.CHANGE_ITEM_CART_COUNT:
      const filteredCartItem = state.cartItems
        .map((item) => {
          if (item.id === action.data.id && item.count !== 0) {
            item.count += action.data.count;
          }
          return item;
        })
        .filter((item) => {
          return item.count != 0;
        });

      return { ...state, cartItems: filteredCartItem };
    default:
      return state;
  }
};

export default cartReducer;
