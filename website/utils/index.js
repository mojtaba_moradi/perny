import authorization from "./authorization";
import dictionary from "./dictionary";

const utils = { authorization, dictionary };
export default utils;
