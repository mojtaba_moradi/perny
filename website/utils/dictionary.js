const dictionary = (text) => {
  let translated;
  let lowerText = text?.toLowerCase();
  switch (lowerText) {
    case "latest":
      translated = "جدیدترین ها";
      break;
    case "offer":
      translated = "پیشنهادات ویژه";
      break;
    case "mostdiscount":
      translated = "بیشترین تخفیف ها";
      break;
    case "cheapest":
      translated = "ارزان ترین ها";
      break;
    case "expensive":
      translated = "گران ترین ها";
      break;
    default:
      translated = text;
      break;
  }
  // console.log({ dictionary: text, translated });

  return translated;
};

export default dictionary;
