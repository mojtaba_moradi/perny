import Cookie from "js-cookie";
const authorization = () => {
  return Cookie.get("PernyUserToken") !== undefined;
};
export default authorization;
