const ButtonComponent = ({ bgColor, title, style, click, disabled }) => {
  return (
    <div disabled={disabled} onClick={click} style={style} className={`w-btn ${bgColor}`}>
      <i />
      <span>{title}</span>
    </div>
  );
};

export default ButtonComponent;
