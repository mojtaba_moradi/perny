import globalUtils from "../../../globalUtils";

const DiscountPrice = ({ realPrice, newPrice, discount, column }) => {
  return (
    <div className="discount-price-wrapper" style={{ flexDirection: column ? "column" : "" }}>
      {" "}
      {discount ? (
        <div className="product-info-wrapper textEnd">
          {newPrice ? <del className="realPrice "> {globalUtils.formatMoney(realPrice)}</del> : ""}
          {discount ? (
            <div className="discount-product">
              {" "}
              <span>%{discount}</span>
            </div>
          ) : (
            ""
          )}{" "}
        </div>
      ) : (
        ""
      )}
      <div className="price-wrapper textEnd">
        <h4>
          {globalUtils.formatMoney(newPrice || realPrice)}
          <span>{" ریال "}</span>
        </h4>
      </div>
    </div>
  );
};

export default DiscountPrice;
