import jalaliDateConvert from "../../../../panelAdmin/utils/jalaliDateConvert";
import Star from "../../../../panelAdmin/component/UI/Rating/Star";
import Comment from "../../../reusableComponent/Comment";
import Link from "next/link";

const { default: ProductReview } = require("../../productScreen/ProductReview");

const SingleBlog = ({ datas }) => {
  console.log({ singleBlogData: datas });
  function ShowHtml(props) {
    return <div dangerouslySetInnerHTML={{ __html: props }} />;
  }
  let dateAndClock = jalaliDateConvert(datas?.createdAt, true);

  return (
    <section className="articles-main-area">
      <ul className="article-back-btn">
        <li>
          <a href="/blogs">{"بازگشت"}</a>
        </li>
      </ul>
      <hr />
      <div className="article-area">
        <div className="article-top-section">
          <h1>{datas.title} </h1>
          <div className="time-date">
            <span>
              <i class="fal fa-clock"> </i>
            </span>
            <span className="date">
              {" "}
              {dateAndClock.clock} - {dateAndClock.date}
            </span>
            <span></span>
            <span className="time"></span>
          </div>
          {/* <div className="time-for-reading">
            <span>زمان مورد نیاز برای مطالعه: </span>
            <span>۱ دقیقه </span>
            <span>
              <i class="fal fa-stopwatch"></i>
            </span>
          </div> */}
        </div>
        <div className="article-main-content">
          <img src={datas.thumbnail} alt="" />
          <div className="article-content">{ShowHtml(datas.content)}</div>
        </div>
        <div className="article-footer-section">
          <div>
            <Star rating={datas?.rate} fixed />
          </div>
          <div className="share">
            <span className="comments">
              <i></i>
              <span>۵</span>
            </span>

            <div className="socials">
              {/* <a href="">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="">
                <i class="fab fa-facebook-f"></i>
              </a> */}
              <a href="https://www.instagram.com" target="__blank">
                <i class="fab fa-instagram"></i>
              </a>

              <a target="__blank" href="https://www.linkedin.com">
                <i class="fab fa-linkedin-in"></i>
              </a>
              <a target="__blank" href="https://web.telegram.org">
                <i class="fab fa-telegram-plane"></i>
              </a>
            </div>
          </div>
        </div>
        <div className="article-category">
          <p>دسته بندی:</p>
          <span>اخبار</span>
        </div>
      </div>
      <div className="comments-area">
        <div className="add-comment-area"></div>
        <Comment data={datas} parentType={"Blog"} />
      </div>
    </section>
  );
};

export default SingleBlog;
