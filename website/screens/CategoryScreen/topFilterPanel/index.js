import website from "../../..";

const TopFillterPanel = ({ click, datas }) => {
  return (
    <div className="top-filter-section">
      <ul>
        {datas ? (
          <li onClick={click}>
            <div className="one-filter-wrapper">
              <span>{website.utils.dictionary(datas)}</span>
              <span>
                <i className="fal fa-times"></i>
              </span>
            </div>
          </li>
        ) : (
          <></>
        )}
      </ul>
    </div>
  );
};

export default TopFillterPanel;
