import React from "react";
import Link from "next/link";
import LazyImage from "../../../../../components/LazyImage";
import Star from "../../../../../panelAdmin/component/UI/Rating/Star";
import DiscountPrice from "../../../../component/DiscountPrice";
const ProductsSectionCard = (props) => {
  const { _id, images, name, realPrice, newPrice, discount, rate } = props;

  return images?.length || realPrice || name ? (
    <React.Fragment>
      <li>
        <Link
          // href={`/product?productInfo=${props}`}
          href={`/product?productId=${props._id}`}
          // href={`/product/[id]`}
          // as={`/product/${_id}`}
        >
          <a>
            <figure>
              <div className="product-image-wrapper">
                <LazyImage src={images?.length ? images[0] : null} alt={name} />
              </div>

              <figcaption>
                <div title={name} className="product-info-wrapper">
                  <h1>{name}</h1>
                </div>
                <div className="product-info-wrapper product-star-rating">
                  <Star rating={rate} fixed />
                </div>
                <DiscountPrice {...{ realPrice, newPrice, discount }} />
              </figcaption>
            </figure>
          </a>
        </Link>
      </li>
    </React.Fragment>
  ) : (
    ""
  );
};

export default ProductsSectionCard;
