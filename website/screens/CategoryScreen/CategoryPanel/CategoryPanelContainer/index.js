import React, { useState } from "react";

const CategoryPanelContainer = ({ datas, click, activeTitle }) => {
  console.log({ datas });
  return (
    <div className="category-panel-section ">
      <h6> دسته بندی ها</h6>
      <ul className={"show-all"}>
        {datas?.map((data, index) => (
          <li key={`category-${index}`}>
            <p className={`pointer transition0-2 filterAmountTitle ${activeTitle === data.name ? "active" : ""}`} onClick={() => click({ id: data._id, name: data.name })}>
              {data.name}
            </p>
          </li>
        ))}
      </ul>
      {/* <button onClick={onClickHandler}>{!button ? "+نمایش بیشتر" : "−نمایش کمتر"}</button> */}
    </div>
  );
};

export default CategoryPanelContainer;
