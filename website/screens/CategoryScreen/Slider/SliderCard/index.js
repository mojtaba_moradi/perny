import React from "react";
import LazyImage from "../../../../../components/LazyImage";

const SliderCard = (props) => {
  const { image } = props;

  return (
    <div className="slider-card-wrapper">
      {/* <LazyImage src={image} alt={"slider"} /> */}
      <img src={image} alt="1" />
    </div>
  );
};

export default SliderCard;
