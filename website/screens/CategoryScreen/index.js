import BackgrandCover from "../../../panelAdmin/component/UI/BackgrandCover";
import LoadingCircelDot from "../../../panelAdmin/component/UI/Loadings/LoadingCircelDot";
import SliderContainer from "./Slider/SliderContainer";
import TopFillterPanel from "./topFilterPanel";
import CategoryPanelContainer from "./CategoryPanel/CategoryPanelContainer";
import ProductsSectionContainer from "./productsSection/ProductsSectionContainer";
import FilterPanelContainer from "./filtersPanel/FilterPanelContainer";
import { useEffect, useState } from "react";

const CategoryScreen = ({ categoryClickHandler, res, seeProducts, isLoading, seeAllProductThisPage, setCategoryMenu, categoryMenu, catData, filterState, catDataRemoverFilters, catDataRemoverCategories, categoryState, acceptedCat, filterClickHandler }) => {
  return (
    <div className="base-container">
      <BackgrandCover onClick={() => setCategoryMenu(false)} fadeIn={categoryMenu} />

      {isLoading && (
        <div className="staticStyle bgDark">
          <LoadingCircelDot />
        </div>
      )}
      <section className="categories-section">
        <div className="left-side-categories">
          <SliderContainer data={catData?.sliders || catData?.sliders} />
          <div className="cat-menu-toggler" onClick={() => setCategoryMenu(!categoryMenu)}>
            <p>دسته بندی و فیلترها</p>
          </div>
          <div className="top-filter-cat-section">
            <TopFillterPanel click={catDataRemoverFilters} datas={filterState} />
            <TopFillterPanel click={catDataRemoverCategories} datas={categoryState} />
          </div>
          <ProductsSectionContainer seeAllProductThisPage={seeAllProductThisPage} data={catData?.cat?.docs} />
          <span ref={seeProducts}></span>
        </div>
        {/* transition0-2 */}
        <div className="right-side-categories transition0-1 ">
          <div className={`stickyOn transition0-1  ${categoryMenu ? "cat-fil-wrapper show-cat" : "cat-fil-wrapper"}`}>
            <CategoryPanelContainer activeTitle={acceptedCat?.name} datas={res?.allCat} click={categoryClickHandler} />
            <FilterPanelContainer activeTitle={filterState} click={filterClickHandler} />
          </div>
        </div>
      </section>
    </div>
  );
};

export default CategoryScreen;

// useEffect(() => {
//   const stickyDiv = document.querySelector(".stickyOn");
//   window.addEventListener("scroll", function () {
//     stickyDiv.style.top = Math.ceil(window.pageYOffset) + 10 + "px";
//   });
//   setState(window.pageYOffset);
// });
// console.log({ state });
