import React, { useState } from "react";
import ProductDesc from "../ProductDesc";
import Comment from "../../../reusableComponent/Comment";

const ProductNav = ({ data }) => {
  const [show, setShow] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const showHandler = (e) => {
    setShow(e);
  };
  return (
    <>
      <div className="nav-wrapper">
        <div className="product-tab">
          <ul className="product-nav">
            <li>
              <button className={!show ? "active" : "disable"} onClick={() => showHandler(false)}>
                <i className="fad fa-comment-alt" />
                <span> توضیحات</span>
              </button>
            </li>
            <li>
              <button className={show ? "active" : "disable"} onClick={() => showHandler(true)}>
                <i className="fad fa-comments" />
                <span>نظرات</span>
              </button>
            </li>
          </ul>
        </div>
        <div className="desc-review-wrapper">
          <div className={show ? "desc-wrapper no-height" : "desc-wrapper"}>
            <ProductDesc />
          </div>
          <div className={!show ? "review-wrapper no-height" : "review-wrapper"}>
            <Comment data={data} parentType={"Product"} />
          </div>
        </div>
      </div>
    </>
  );
};
export default ProductNav;
