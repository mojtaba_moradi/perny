import formatMoney from "../../../../../panelAdmin/utils/formatMoney";
import AddButton from "../../../../component/ui/AddButton";

const SidebarShoppingCart = ({ dispatch, state }) => {
  return (
    <div className="right-side-item-list">
      <ul>
        <li>
          <figure>
            <figcaption>
              <p>تعداد کالا : </p>
              <div className="prices">
                <span>{state?.shoppingCart?.length}</span>
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure>
            <figcaption>
              <p>تعداد کل کالا ها : </p>
              <div className="prices">
                <span>{state?.totalGoods}</span>
              </div>
            </figcaption>
          </figure>
        </li>
        <li>
          <figure>
            <figcaption>
              <p>جمع کل :</p>
              <div className="prices">
                <span>{formatMoney(state?.totalPrice)}</span>
                <span>{" ریال "}</span>
              </div>
            </figcaption>
          </figure>
        </li>
      </ul>
      <div className="accepted-order-in-show-cart">
        <AddButton
          // click={click}
          color={"blue"}
          title={"ثبت سفارش"}
        />
      </div>
    </div>
  );
};

export default SidebarShoppingCart;
