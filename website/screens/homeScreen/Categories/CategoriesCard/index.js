import React from "react";
import Link from "next/link";
import LazyImage from "../../../../../components/LazyImage";

const CategoriesCard = (props) => {
  const { _id, image, name } = props;

  return (
    <React.Fragment>
      <li>
        <figure>
          {/* <img src={image} alt="" /> */}
          <figcaption>
            <div>
              <LazyImage src={image} alt={name} />
            </div>
            <div>
              <h2 className="title">{name}</h2>
              <Link href={`/category?id=${_id}`} as={`/category?id=${_id}`}>
                <a onClick={() => localStorage.setItem("selectedCategory", name)} className="details-btn">
                  {"جزئیات"}
                </a>
              </Link>
            </div>
          </figcaption>
        </figure>
      </li>
    </React.Fragment>
  );
};

export default CategoriesCard;
