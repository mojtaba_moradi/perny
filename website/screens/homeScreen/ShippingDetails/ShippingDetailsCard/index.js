import React from "react";
import costumer from "../../../../public/images/costumer.jpg";
import price from "../../../../public/images/price.jpg";
import pay from "../../../../public/images/pay.jpg";
import shipping from "../../../../public/images/shipping.jpg";

const ShippingDetailsCard = () => {
  return (
    <section className="shipping-section">
      <div className="shipping-wrapper">
        <ul>
          <li>
            <figure>
              <img src={price} alt="price" />
              <figcaption>
                <h6>ارزانترین قیمت</h6>
                <p>تضمین ارزانترین قیمت بازار</p>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src={pay} alt="pay" />

              <figcaption>
                <h6>پرداخت در محل</h6>
                <p></p>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src={costumer} alt="customer" />

              <figcaption>
                <h6>سفارش تلفنی</h6>
                <p>۰۲۱−۸۲۳۹۱</p>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src={shipping} alt="shipping" />

              <figcaption>
                <h6>ارسال رایگان</h6>
                <p>بالای ۲۵۰ هزار تومان</p>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default ShippingDetailsCard;
