const ContactUs = () => {
  return (
    <section className="contact-us base-container">
      <h1>تماس با ما</h1>
      <p>
        شما همراهان گرامی می توانید سوالات، نظرات و هر گونه انتقاد و پیشنهادات
        خود را از راه های ارتباطی زیر با ما در میان بگذارید .
      </p>
      <p>نظرات و پیشنهادات شما راهنمای ما برای ادامه راه خواهد بود</p>
      <p>
        تلفن اطلاع رسانی : <span>09033330652</span>
      </p>
      <p>
        آدرس : <span> رشت گلسار خیابان استاد معین نبش کوچه ۲۹ پلاک ۱۲</span>
      </p>
      <div className="social-section">
        <p>شبکه های اجتماعی :</p>
        <ul className="social">
          <li>
            <a href="">
              <i class="fab fa-telegram"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="fab fa-instagram"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
          <li>
            <a href="">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default ContactUs;
