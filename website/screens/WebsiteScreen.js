import React, { Fragment, useContext } from "react";
import HeaderMain from "../baseComponent/HeaderMain/HeaderMainCard";
import Footer from "../baseComponent/Footer/FooterCard";
import WithErrorHandler from "../utils/userHoc/WithErrorHandler";
import { useEffect } from "react";
import reducer from "../../_context/reducer";
import ContextCheck from "./ContextCheck";

const WebsiteScreen = (props) => {
  const ShoppingCartProvider = reducer.webSiteReducer.shoppingCartReducerProvider;
  const AuthenticationProvider = reducer.webSiteReducer.authenticationReducerProvider;

  useEffect(() => {
    !(function () {
      function t() {
        var t = document.createElement("script");
        (t.type = "text/javascript"), (t.async = !0), localStorage.getItem("rayToken") ? (t.src = "https://app.raychat.io/scripts/js/" + o + "?rid=" + localStorage.getItem("rayToken") + "&href=" + window.location.href) : (t.src = "https://app.raychat.io/scripts/js/" + o);
        var e = document.getElementsByTagName("script")[0];
        e.parentNode.insertBefore(t, e);
      }
      var e = document,
        a = window,
        o = "946e8a15-6dd7-4446-aae3-b185f4a8498f";
      "complete" == e.readyState ? t() : a.attachEvent ? a.attachEvent("onload", t) : a.addEventListener("load", t, !1);
    })();
  });
  return (
    <AuthenticationProvider>
      <ShoppingCartProvider>
        <ContextCheck>
          <Fragment>
            <HeaderMain />
            {props.children}
            <Footer />
          </Fragment>
        </ContextCheck>
      </ShoppingCartProvider>
    </AuthenticationProvider>
  );
};

export default WebsiteScreen;
