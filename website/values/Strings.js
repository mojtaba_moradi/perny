export const ERNTER_SIX_CODE = "کد وارد شده باید شش رقمی باشد";
export const ENTER_TO_PERNY = "ورود به پرنی مارکت";
export const SOMETHING_WENT_WRONG = "خطایی رخ داده است";
export const ENTER_CORRECT_NUMBER = "شماره همراه خود را به طور صحیح وارد نمایید";
export const MENU_DATA = (location) => {
  let data;
  return (data = [
    { title: "خانه", link: "/", active: location === "/" },
    { title: "جدیدترین ها", link: "/category?filter=latest", active: location.includes("latest") },
    { title: "پیشنهادات ویژه", link: "/category?filter=offer", active: location.includes("offer") },
    { title: "تخفیف ها", link: "/category?filter=mostDiscount", active: location.includes("mostDiscount") },
    { title: "سوالات متداول", link: "/Questions", active: location.includes("/Questions") },
    { title: "مقالات", link: "/blogs", active: location.includes("/blogs") },
    { title: "درباره ما", link: "/about", active: location.includes("/about") },
    { title: "تماس با ما", link: "/contactUs", active: location.includes("/contactUs") },
  ]);
};
