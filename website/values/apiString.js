const HOME_SCREEN = "/homeScreen";
const CATEGORY = "/category";
const PRODUCT = "/product";
const COMMENT = "/comment";
const BLOG_CATEGORY = "/blog/category";
const apiString = {
  HOME_SCREEN,
  CATEGORY,
  PRODUCT,
  COMMENT,
  BLOG_CATEGORY,
};
export default apiString;
