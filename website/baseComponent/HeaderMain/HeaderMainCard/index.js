import React, { useRef, useState, useContext } from "react";
import Link from "next/link";
import logo from "../../../public/images/logo.png";
import { getFilters } from "../../../../website/userApi/get/filter";
import { MENU_DATA } from "../../../values/Strings";
import BackgrandCover from "../../../../panelAdmin/component/UI/BackgrandCover";
import Navbar from "../Navbar";
import LoginModal from "../../LoginModal";
import website from "../../..";
import HeaderUserOption from "./HeaderUserOption";
import reducer from "../../../../_context/reducer";

const HeaderMain = () => {
  // ======================== context
  const websiteContext = reducer.webSiteReducer;
  const shoppingCart = useContext(websiteContext.shoppingCartReducerContext);
  const authentication = useContext(websiteContext.authenticationReducerContext);
  const { dispatch: shoppingCartDispatch, state: shoppingCartState } = shoppingCart;
  const { dispatch: authenticationDispatch, state: authenticationState } = authentication;
  // console.log({ shoppingCartDispatch, shoppingCartState, authenticationDispatch, authenticationState });
  // ======================== End context
  //=========================== STATE=============================
  const [menuState, setMenuState] = useState(false);
  const [exitState, setExitState] = useState(false);
  const [searchingData, setSearchingData] = useState(null);
  const [searchDropDown, setSearchDropDown] = useState(false);
  const [showSmallDeviceSearch, setShowSmallDeviceSearch] = useState(false);
  const [show, setShow] = useState(false);
  //=========================== STATE=============================
  //============================ VARIABLES =======================
  let menuBtn = useRef();
  let menu = useRef();
  let searchRef = useRef(null);
  //============================ VARIABLES =======================

  // =========================== Handlers =======================
  const openMenu = () => {
    setMenuState(!menuState);
  };
  const closeMenu = () => {
    setMenuState(!menuState);
  };
  const exitHandler = () => {
    setExitState(!exitState);
  };
  const _handleSignOut = () => {
    // Cookies.remove("PernyUserToken");
    authenticationDispatch.removeToken();
    setExitState(!exitState);
  };

  const searchOnChangeHandler = (e) => {
    if (e.currentTarget.value.length >= 3) {
      searching(e.currentTarget.value);
      setSearchDropDown(true);
    } else {
      setSearchDropDown(false);
    }
  };
  const searching = async (e) => {
    const searchProducts = await getFilters(`/product?page=1&search=${e}`);
    console.log({ dingding: searchProducts.data.docs });
    setSearchingData(searchProducts);
  };

  const userMobile = () => {
    return authenticationState.authentic ? (
      <HeaderUserOption {...{ _handleSignOut, shoppingCartState, shoppingCartDispatch }} />
    ) : (
      <div className="greeting transition0-2 pointer" onClick={() => setShow(true)}>
        {"ورود / ثبت نام"}
      </div>
    );
  };
  // ++++++++++++++++++ deleting cash ++++++++++++++++++++++++++++++++

  // ***************************** OUR HTML *****************************
  return (
    <header className="base-container ">
      <BackgrandCover onClick={() => setMenuState(!menuState)} fadeIn={menuState} />
      <div className="header-wrapper">
        <div className={menuState ? "overlay over-show" : "overlay over-unShow"}></div>
        <div className="top-section">
          <div className="right-side-wrapper">
            <Link href="/">
              <a>
                <img src={logo} alt="perny-market" />
              </a>
            </Link>
          </div>
          <div className="left-side-wrapper">
            {userMobile()}
            <div className="search">
              <form className="form-search">
                <i className="fal fa-search"></i>
                <input className="form-search-input" placeholder="شما میتوانید محصول مورد نظر خودتان را جستجو کنید" type="search" placeholder="" aria-label="Search" ref={searchRef} onChange={searchOnChangeHandler} />
              </form>
              <div className={searchDropDown ? "search-drop-down show-search" : "search-drop-down"}>
                {searchingData?.data?.docs?.map((data) => (
                  <Link href={`/product?productId=${data._id}`}>
                    <a key={data._id} onClick={() => setSearchDropDown(false)}>
                      <figure>
                        <img src={data?.images[0]} alt={data?.name} />
                        <figcaption>
                          <p>{data?.name}</p>

                          <p>
                            {data.realPrice}
                            <span> ریال</span>
                          </p>
                        </figcaption>
                      </figure>
                    </a>
                  </Link>
                ))}
              </div>
            </div>
          </div>
        </div>
        <Navbar {...{ menuBtn, setShowSmallDeviceSearch, openMenu, showSmallDeviceSearch, searchRef, searchOnChangeHandler, menu, searchDropDown, searchingData, MENU_DATA, userMobile, setMenuState, menuState, closeMenu }} />
      </div>
      <LoginModal {...{ show, setShow }} />
    </header>
  );
};

export default HeaderMain;
