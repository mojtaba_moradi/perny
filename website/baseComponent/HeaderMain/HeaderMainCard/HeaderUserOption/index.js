import { useState, useRef, useEffect } from "react";
import Link from "next/link";
import Swal from "sweetalert2";
import formatMoney from "../../../../../panelAdmin/utils/formatMoney";
import AddButton from "../../../../component/ui/AddButton";
import { useRouter } from "next/router";

const HeaderUserOption = ({ _handleSignOut, shoppingCartState, shoppingCartDispatch }) => {
  const router = useRouter();
  const [modal, setModal] = useState(false);
  const [shoppingCartComponent, setShoppingCartComponent] = useState(false);
  const wrapperRef = useRef(null);
  function _handelModal() {
    setModal(!modal);
  }
  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      _handelModal();
    }
  };
  useEffect(() => {
    if (modal) {
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }
    if (router?.route.includes("shoppingCart")) setShoppingCartComponent(true);
    else setShoppingCartComponent(false);
  });
  const swalTrueFalse = (id, index) => {
    Swal.fire({
      title: shoppingCartState?.shoppingCart[index].name,
      text: "آیا مطمئن به حذف این محصول از سبد خود هستید!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "بله, حذف میکنم!",
      cancelButtonText: "خیر",
    }).then((result) => {
      console.log({ swal: id });
      if (result.value) {
        shoppingCartDispatch.removeProduct(id);
        Swal.fire({
          position: "top-start",
          icon: "success",
          title: "با موفقیت از سبد خرید شما حذف شد",
          showConfirmButton: false,
          timer: 2000,
        });
      }
    });
  };
  return (
    <div className="greeting transition0-2">
      <div ref={wrapperRef} className="user-icon-angel transition0-2" onClick={_handelModal}>
        <i className="fas fa-user"></i>
        <i className="fas fa-angle-down"></i>

        <div className={`modal-for-user ${modal ? "show" : ""}`}>
          <div onClick={_handleSignOut}>
            <i className=" fas fa-sign-out-alt"></i>
            <span>{"خروج"}</span>
          </div>
        </div>
      </div>
      <div className={`user-shopping-cart ${shoppingCartComponent ? "disable" : ""}`}>
        <i className="fas fa-shopping-cart"></i>
        {shoppingCartState?.shoppingCart?.length ? (
          <>
            {" "}
            <div className={`modal-for-basket`}>
              <div className="modal-basket-row">
                <span className="modal-shopping-cat-length">
                  {" "}
                  {shoppingCartState?.shoppingCart?.length}
                  {"کالا"}
                </span>
                <Link href={"/shoppingCart"}>
                  <a>{"مشاهده سبد خرید"}</a>
                </Link>
              </div>
              {shoppingCartState?.shoppingCart?.map((basket, index) => {
                return (
                  <div className="modal-basket-row pointer hoverBg">
                    <Link href={`/product?productId=${basket._id}`} as={`/product?productId=${basket._id}`}>
                      <a className="">
                        <div className="">
                          <img src={basket?.images?.length && basket?.images[0]} />
                        </div>
                        <div className="modal-product-name">
                          <p>{basket?.name}</p>
                          <div className="product-length-and-remove">
                            <div className="product-order-number">{`عدد ${basket?.orderNumber || 0}`}</div>
                          </div>
                          {/* <div>{basket?.newPrice || basket?.realPrice}</div> */}
                        </div>
                      </a>
                    </Link>
                    <i onClick={() => swalTrueFalse(basket._id, index)} title="حذف " className="fad fa-trash-alt"></i>
                  </div>
                );
              })}
              <div className="show-total-price-and-seller">
                <div className="modal-total-price">
                  <span className="total-price-title">مبلغ قابل پرداخت</span>
                  {/* .slice(0, -1) */}
                  {/* formatMoney(?.reduce(checkTotalPrice)) */}
                  <div>
                    <span className="total-price">{formatMoney(shoppingCartState.totalPrice)}</span>
                    <span className="the-unit-title">{"ریال"}</span>
                  </div>
                </div>
                <Link href="/shoppingCart">
                  <a>
                    <AddButton color={"blue"} title={"سبد خرید"} small />
                  </a>
                </Link>
              </div>
            </div>
            <span className="show-shopping-cart-length">{shoppingCartState?.shoppingCart?.length}</span>
          </>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default HeaderUserOption;
